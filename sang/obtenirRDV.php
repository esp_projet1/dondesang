<?php
session_start();
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">

        <title>
            Dons de sang
        </title>

        <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
        <link rel="stylesheet" href="css/style.css"> <!-- Gem style -->
        <link rel="stylesheet" type="text/css" href="styles/base.css" media="all" />
        <link rel="stylesheet" type="text/css" href="styles/gabarit01.css" media="screen" />
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <script src="js/modernizr.js"></script> <!-- Modernizr -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

        <script src="js/jquery-3.1.0.min.js"></script>
        <script src="js/main.js"></script>

    </head>

    <body>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <div id="global">


        <div id="entete">
            <div>

                <div class="principal">
                    <div class="principal_petit">
                        <div class="principal_img">
                            <a href="https://plus.google.com">
                                <img src="google2.png" border="0" align="center" width="98%"/>
                            </a>
                        </div>
                    </div>
                </div>



            </div>

            <div style="position: absolute; left: 90px; top: 15px">

                <div class="principal">
                    <div class="principal_petit">
                        <div class="principal_img">
                            <a href="http://www.facebook.com">
                                <img src="facebook2.png" border="0" align="center" width="98%"/>
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <div style="position: absolute; left: 160px; top: 15px">

                <div class="principal">
                    <div class="principal_petit">
                        <div class="principal_img">
                            <a href="http://www.instagram.com">
                                <img src="insta.png" border="0" align="center" width="99%"/>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <span style="position: absolute; right: 550px; top: 5px; font-family: arial">
			<h2 style="color: white">
				Planifiez vos dons
			</h2>
		</span>
            <nav class="main-nav">
                <ul>
                    <!-- inser more links here -->
                    <li id="test"><a class="cd-signup" href="#0">Gérer Compte</a></li>
                    <li id="test"><a class="cd-signup" href="deconnexion.php">Déconnexion</a></li>



                </ul>
            </nav>

        </div> <!-- Fin entete -->
        <div id="navigation">
            <a href="profil.php"><img src="sang.jpg" width="8%" style="position: absolute; left: 25px ;top : 100px"></a>

            <style>
                .button {
                    position: relative;
                    float: left;
                    left: 0;
                    background-color: red;
                    border: none;
                    color: white;
                    padding: 0.5em;
                    text-align: center;
                    text-decoration: none;
                    display: inline-block;
                    font-size: 16px;
                    margin: 10px 35px;
                    -webkit-transition-duration: 0.4s;
                    transition-duration: 0.4s;
                    cursor: pointer;
                    border-radius: 25px;
                }


                .button1 {
                    background-color: white;
                    color: black;
                    border: 2px solid #D40015;
                    border-radius: 25px;
                }

                .button1:hover {
                    background-color: #D40015;
                    color: white;
                }


                .buttonRDV:enabled{
                    color: white;
                    background-color: #D40015;
                }

            </style>
            <div style="position: absolute; top: 125px; font-family: sans-serif">
                <a href="profil.php"><button class="button button1">ACCUEIL COMPTE</button></a>
                <a href="obtenirRDV.php"><button class="button button1 buttonRDV">OBTENIR RENDEZ-VOUS</button></a>
                <a href="notif_donneur.php"><button class="button button1">CONSULTER NOTIFICATIONS</button></a>

            </div>




            <div align="center">
                <div style="width:275px; position: absolute; right: 3px; top: 130px">
                    <div>
                        <input type="text" name="user_search" placeholder="RECHERCHER" id="area_search">
                        <button type="button" id="btn_search"><img src="search.png" width="40%"></button>

                    </div>
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    $('#btn_search').click(function()
                    {
                        $("#area_search").css({
                            "height": "50px",
                            "border": "0",
                            "transition": "all 0.2s ease-in",
                            "visibility": "visible",
                            "background-color": "#DD4545",
                            "width": "200px"
                        });
                        $("#btn_search").css({
                            "background-color": "#999999"
                        });
                    });
                    $("#area_search").keypress(function(){
                        $("#sous_search").css({
                            "transition": "all 0.1s ease-in",
                            "visibility": "visible",
                            "height": "25px"
                        });
                    });
                });
            </script>

        </div>


        <div id="contenu">

<?php
if (isset($_SESSION['mel'])  && !empty($_SESSION['mel']) )
{
    $BD= new PDO("mysql:host=localhost; dbname=dondesang", "root", "Moimeme2018");
    $requet='SELECT * FROM donneurs where email=?';
    $req = $BD->prepare($requet);
    $req->execute(array($_SESSION['mel']));
    $resultat=$req-> rowCount();

    if($resultat!=0) {

        while ($donnes = $req->fetch()) {

            echo '<h2 style="position: absolute; top: 300px; left: 75px; color: grey">Donneur ' . $donnes['prenom_donneur'] . ' ' . $donnes['nom_donneur'] . '</h2>
                <img src="heart.png" width="75px" style="position:absolute; left: 500px; top: 275px">
                <br>';
            echo '<b><h4 style="position: absolute; right: 50px; top: 61px; color: grey"> Bienvenue ' . $donnes['prenom_donneur'] . ' ' . $donnes['nom_donneur'] . '</h4></b>';
            ?>

            <div style="border: solid #E6E5E5;background: #0090FE; width: 48%; -webkit-border-radius: ;-moz-border-radius: ;border-radius: 5px ;">
                <br><br>
                <span style="color: whitesmoke;">
                <span style="font-size: 25px">C</span>HOISIR <span style="font-size: 25px">M</span>ON <span
                            style="font-size: 25px">C</span>ENTRE <span style="font-size: 25px">D</span>E <span
                            style="font-size: 25px">D</span>ON
                </span>
                <br><br><br><br><br><br>
                <form method="post" action="obtenirRDV.php">
                    <select placeholder="Choisir centre" name="centrerdv" style="width: 75%; margin-left:75px;"
                            required>
                        <option value="">

                        </option>
                        <optgroup label="DAKAR">
                            <option value="Centre National de Transfusion Sanguine">
                                Centre National de Transfusion Sanguine
                            </option>
                            <option value="Centre hospitalier regional le Dantec">
                                Centre hospitalier régional le Dantec
                            </option>
                            <option value="Hopital principal">
                                Hopital principal
                            </option>
                            <option value="Hopital Roi Baudouin">
                                Hopital Roi Baudouin
                            </option>
                        </optgroup>
                        <optgroup label="THIÈS">
                            <option value="Hopital Regional de Thies">
                                Hopital Regional de Thies
                            </option>
                            <option value="Hopital Saint Jean de Dieu">
                                Hopital Saint Jean de Dieu
                            </option>
                            <option value="Hopital de Mbour">
                                Hopital de Mbour
                            </option>
                        </optgroup>
                        <optgroup label="SAINT-LOUIS">
                            <option value="Hopital Regional de Saint-Louis ">
                                Hopital Regional de Saint-Louis
                            </option>
                            <option value="Hopital de Ndioum">
                                Hopital de Ndioum
                            </option>
                        </optgroup>
                        <optgroup label="DIOURBEL">
                            <option value="Hopital de Diourbel">
                                Hopital de Diourbel
                            </option>
                            <option value="Hopital Matlaboul Fawzeyni de Touba">
                                Hopital Matlaboul Fawzeyni de Touba
                            </option>
                        </optgroup>
                        <optgroup label="KAOLACK">
                            <option value="Hopital El Hadj Ibrahima NIASS">
                                Hopital El Hadj Ibrahima NIASS
                            </option>
                        </optgroup>
                        <optgroup label="LOUGA">
                            <option value="Hopital regional de Louga">
                                Hopital regional de Louga
                            </option>
                        </optgroup>
                        <optgroup label="MATAM">
                            <option value="Hopital de Ourossogui">
                                Hopital de Ourossogui
                            </option>
                        </optgroup>
                        <optgroup label="BAKEL">
                            <option value="District sanitaire de Bakel">
                                District sanitaire de Bakel
                            </option>
                        </optgroup>
                        <optgroup label="ZIGUINCHOR">
                            <option value="Hopital de Ziguinchor">
                                Hopital de Ziguinchor
                            </option>
                        </optgroup>
                    </select>

                    <br><br><br><br><br><br><br><br><br>
            </div>
            <div style="border: solid #E6E5E5; width: 48%; background: #0090FE; position: absolute; right: 20px; top: 475px; -webkit-border-radius: ;-moz-border-radius: ;border-radius: 5px;: ;">
                <br><br>
                <span style="color: whitesmoke;">
                <span style="font-size: 25px">C</span>HOISIR <span style="font-size: 25px">M</span>ON <span
                            style="font-size: 25px">R</span>ENDEZ-<span style="font-size: 25px">V</span>OUS
                </span>
                <br><br>


                <table border="1">
                    <tr align="center">
                        <th width="150px" style="text-align:center;">
                            <span style="position:absolute;left: 0; top: 105px"><strong>Type de Don: Sang</strong></span>
                            <br><br>
                        </th>


                        <th>
                <span style="position: relative; margin-right: 110px;">
                    <strong>Nom salle:</strong><select style="width: 63%" name="sallerdv">

                    <option value="">

                    </option>
                    <option value="A">
                        A
                    </option>
                    <option value="B">
                        B
                    </option>
                    <option value="C">
                        C
                    </option>
                </select></span>
                            <br>
                        </th>
                    </tr>


                    <tr>
                        <td>

                            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                            <link rel="stylesheet" href="/resources/demos/style.css">
                            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                            <script>
                                $(function () {
                                    $("#datepicker").datepicker();
                                });
                            </script>

                            <p><strong>Date du rendez-vous:</strong> <input type="text" id="datepicker" name="daterdv"
                                                                            placeholder="Format MM/JJ/AAAA"></p>
                        </td>


                        <td align="center">
                            <strong style="margin-right:250px;">Heure:</strong>
                            <span style="position:absolute; width: 48%;right: 10px; top: 147px">
                    <select style="width: 60%" name="heurerdv">
                    <option value="">

                    </option>
                    <option value="08:00">
                        08:00
                    </option>
                    <option value="10:00">
                        10:00
                    </option>
                    <option value="12:00">
                        12:00
                    </option>
                    <option value="14:00">
                        14:00
                    </option>
                    <option value="16:00">
                        16:00
                    </option>
                </select>
                 </span>

                        </td>
                    </tr>


                </table>
                <br><br><br><br>
                <a href="valider.php">
                    <input type="submit" value="VALIDER RENDEZ-VOUS" name="ok"
                           style="color: #000A48; background: #A0D3FA; border: none; -webkit-border-radius: ;-moz-border-radius: ;border-radius: 5px;: ;position: absolute;width: 300px;height: 50px; right: 180px; top:275px;">


                </a>

                <br><br><br><br>
                </form>
            </div>
        <?php


        if (isset($_POST['ok']))
        {
        if (validation($_POST['centrerdv']) && validation($_POST['sallerdv']) && validation($_POST['daterdv']) && validation($_POST['heurerdv']))
        {
        if ($_POST) {
        $req1 = 'INSERT INTO rdv(centrerdv, sallerdv, daterdv, heurerdv, id_donneur, nom_du_donneur, prenom_du_donneur, groupSanguin) VALUES (:centrerdv,:sallerdv,:daterdv,:heurerdv,:id_donneur,:nom_du_donneur,:prenom_du_donneur,:groupSanguin)';
        $requete1 = $BD->prepare($req1);
        $requete1->execute(array(
            'centrerdv' => $_POST['centrerdv'],
            'sallerdv' => $_POST['sallerdv'],
            'daterdv' => $_POST['daterdv'],
            'heurerdv' => $_POST['heurerdv'],
            'id_donneur' => $donnes['id'],
            'nom_du_donneur' => $donnes['nom_donneur'],
            'prenom_du_donneur' => $donnes['prenom_donneur'],
            'groupSanguin' => $donnes['groupe_sanguin']
        ));
        if ($requete1) {
        ?>
            <script>
                alert('Votre rendez-vous est enregistré, veuillez attendre la confirmation de l\'administrateur.');
            </script>
        <?php
        echo '<meta http-equiv="refresh" content="0; url=profil.php">';
        exit();
        }
        }
        else
        {
        ?>
            <script>
                alert('Rendez-vous non validé');
            </script>
            <?php
        }

        }

        }

        }
    }
 }
function validation($val)
{
    if(isset($val) && !empty($val))
    {
        return true;
    }
    return false;
}

/* POur vérifier si le mail est dejà present dans la liste de rendez-vous
function is_inmy_db(PDO $base, $mail)
{
    $requet='SELECT id FROM donneurs where email=?';
    $r = $base->prepare($requet);
    $r->execute(array($mail));
    $resultat=$r-> rowCount();

    if($resultat!=0)
    {
        return true;
    }
    return false;
}
*/
//print_r($BD->errorInfo());

?>




            <br><br><br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br>
            <br><br><br><br><br><br><br><br>


    </body>
</html>
