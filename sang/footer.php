<html>
<head>
<link rel="stylesheet" href="footer.css">




</head>
<body>
<footer id="footer-Section">
  <div class="footer-top-layout">
    <div class="container">
      <div class="row">
        <div class="OurBlog">
          <h4>Notre Site</h4>
          <p>L'application a pour but de ravitailler les banques de Sang.
          Donner son sang est un acte de citoyenneté et un geste noble par lequel le citoyen exprime son appartenance à la communauté.<br>
          Des milliers de nos frères meurent chaque jour par manque de sang. Dépassons nos cultures, nos préjugés<br>
          sur le don de sang car le sang est une denrée rare qui sauve des vies.<br><br>
          <a href="plus.php">En Savoir Plus</a>
          </p><br>
          <div class="post-blog-date">
            <?php
            $date=date("D d m Y");
            print($date);
            ?>
            
          </div>
        </div>
        <div class=" col-lg-8 col-lg-offset-2">
          <div class="col-sm-4">
            <div class="footer-col-item">
              <h4>Adresses et Boite Postal</h4>
              <address>
             101 Hamo 4 Guédiawaye<br>
             Corniche Est Dakar, Sénégal
              </address>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="footer-col-item">
              <h4>Contactez-nous</h4>
              <div class="item-contact"> <a href="tel:+221 78 440 88 22"><span class="link-id">Tel</span>:<span>78 440 88 22</span></a> <a href="tel:+211 33 877 24 08"><span class="link-id">Tel</span>:<span>33 877 24 08</span></a>
              <a href="mailto:abdoucognasy@gmail.com"><span class="link-id">E</span>:<span>sadikhabou@gmail.com</span></a> <br>
              <a href="mailto:sarrelhadj2015@gmail.com"><span class="link-id">E</span>:<span>abougueye96@yahoo.fr</span></a> <br>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="footer-col-item">
              <h4>Newsletter</h4>
              <form class="signUpNewsletter" action="" method="get">
                <input name="" class="gt-email form-control" placeholder="You@youremail.com" type="text">
                <input name="" class="btn-go" value="Go" type="button">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom-layout" style="background: grey">
    <div class="socialMedia-footer" > <a href="#"><img src="goo.png" width="70px"></a> <a href="#"><img src="face.png" width="70px"></a> <a href="#"><img src="insta.png" width="70px"></a> <a href="#"><img src="twit.png" width="70px"></a> </div>
    <div class="copyright-tag" style="color: black">Copyright © 2019 Dons de sang, Tous droits réservés.
    <a href="#" style="position:absolute; bottom: 50px; right: 20px"><img src="haut.png" width="60%"></a></div>
  </div>
</footer>
</body>
</html>
