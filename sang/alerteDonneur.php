<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">

    <title>
        Dons de sang
    </title>

    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
    <link rel="stylesheet" href="css/style.css"> <!-- Gem style -->
    <link rel="stylesheet" type="text/css" href="styles/base.css" media="all" />
    <link rel="stylesheet" type="text/css" href="styles/gabarit01.css" media="screen" />
    <link rel="stylesheet" href="css/estilos.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <script src="js/modernizr.js"></script> <!-- Modernizr -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/jquery-3.1.0.min.js"></script>
    <script src="js/main.js"></script>

</head>

<body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<div id="global">


    <div id="entete">
        <div>

            <div class="principal">
                <div class="principal_petit">
                    <div class="principal_img">
                        <a href="https://plus.google.com">
                            <img src="google2.png" border="0" align="center" width="98%"/>
                        </a>
                    </div>
                </div>
            </div>



        </div>

        <div style="position: absolute; left: 90px; top: 15px">

            <div class="principal">
                <div class="principal_petit">
                    <div class="principal_img">
                        <a href="http://www.facebook.com">
                            <img src="facebook2.png" border="0" align="center" width="98%"/>
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <div style="position: absolute; left: 160px; top: 15px">

            <div class="principal">
                <div class="principal_petit">
                    <div class="principal_img">
                        <a href="http://www.instagram.com">
                            <img src="insta.png" border="0" align="center" width="99%"/>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <span style="position: absolute; right: 550px; top: 30px; font-family: arial">
			<h2 style="color: white">
				Planifiez vos dons
			</h2>
		</span>
        <div style="position: absolute; right: 10px; top: 30px">
            <a href="deconnexion.php"><button value="Déconnexion">Déconnexion</button></a>
        </div>
        <div style="position: absolute; right: 100px; top: 30px">
            <button value="Gérer mon compte">Gérer mon compte</button>
        </div>

    </div> <!-- Fin entete -->
    <div id="navigation">
        <a href="profil.php"><img src="sang.jpg" width="8%" style="position: absolute; left: 25px ;top : 100px"></a>

        <style>
            .button {

                background-color: red;
                border: none;
                color: white;
                padding: 0.5em;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 15px;
                margin: 20px 5px;
                -webkit-transition-duration: 0.4s;
                transition-duration: 0.4s;
                cursor: pointer;
                border-radius: 25px;
            }


            .button1 {
                background-color: white;
                color: black;
                border: 2px solid #D40015;
                border-radius: 25px;
            }

            .button1:hover {
                background-color: #D40015;
                color: white;
            }
        </style>

        <div style="position: absolute; top: 125px; font-family: sans-serif">
            <a href="profil.php"><button class="button button1">ACCUEIL COMPTE</button></a>
            <a href="obtenirRDV.php"><button class="button button1">OBTENIR RENDEZ-VOUS</button></a>
            <a href="alerteDonneur.php"><button class="button button1">CONSULTER NOTIFICATIONS</button></a>
            <a href="#footer"><button class="button button1">A PROPOS</button></a>
        </div>




        <div align="center">
            <div style="width:275px; position: absolute; right: 3px; top: 140px">
                <div>
                    <input type="text" name="user_search" placeholder="RECHERCHER" id="area_search">
                    <button type="button" id="btn_search"><img src="search.png" width="40%"></button>

                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#btn_search').click(function()
                {
                    $("#area_search").css({
                        "height": "50px",
                        "border": "0",
                        "transition": "all 0.2s ease-in",
                        "visibility": "visible",
                        "background-color": "#DD4545",
                        "width": "200px"
                    });
                    $("#btn_search").css({
                        "background-color": "#999999"
                    });
                });
                $("#area_search").keypress(function(){
                    $("#sous_search").css({
                        "transition": "all 0.1s ease-in",
                        "visibility": "visible",
                        "height": "25px"
                    });
                });
            });
        </script>

    </div>


    <div id="contenu">

        <?php
        if (isset($_SESSION['mel'])  && !empty($_SESSION['mel']) )
        {
            $BD= new PDO("mysql:host=localhost; dbname=dondesang", "root", "Moimeme2018",array(PDO::ATTR_ERRMODE=> PDO::ERRMODE_EXCEPTION));
            $requet='SELECT * FROM donneurs where email=?';
            $req = $BD->prepare($requet);
            $req->execute(array($_SESSION['mel']));
            $resultat=$req-> rowCount();

            if($resultat!=0) {

                while ($donnes = $req->fetch()) {

                    echo '<h2 style="position: absolute; top: 300px; left: 75px; color: grey">Donneur ' . $donnes['prenom_donneur'] . ' ' . $donnes['nom_donneur'] . '</h2>
                <img src="heart.png" width="75px" style="position:absolute; left: 500px; top: 275px">
                <br>';
                    echo '<b><h4 style="position: absolute; right: 10px; top: 65px; color: grey"> Bienvenue ' . $donnes['prenom_donneur'] . ' ' . $donnes['nom_donneur'] . '</h4></b>';


                    require_once 'administration/database.php';
                    $r = $BD->query('SELECT * FROM alerte');
                    while ($d = $r->fetch()) {
                        echo '<div class="form-style"><strong>LIBELLÉ ALERTE:</strong> ' . $d['libelle'] . '<br><br>';
                        echo '<strong>DATE ALERTE:</strong> ' . $d['date_alerte'] . '<br><br>';
                        echo '<strong>GROUPE SANGUIN RECHERCHÉ :</strong> ' . $d['groupe'] . '<br><br>';
                        echo '<strong>DESCRIPTIF ALERTE:</strong> <br>' . $d['contenu'] . '<br><br><br><br><br><br></div>';
                    }
                    $r->closeCursor();
                }
            } }
        ?>

        <style type="text/css">
            .form-style{
                max-width: 450px;
                background: #E5EDF2;
                padding: 30px;
                margin: 50px auto;
                box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
                border-radius: 10px;
                border: 6px solid #D40015;
            }
        </style>


        <br><br><br><br><br><br>
        <br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br>








                    <?php

        function validation($val)
        {
            if(isset($val) && !empty($val))
            {
                return true;
            }
            return false;
        }
        function is_inmy_db(PDO $base, $mail)
        {
            $requet='SELECT id FROM donneurs where email=?';
            $r = $base->prepare($requet);
            $r->execute(array($mail));
            $resultat=$r-> rowCount();

            if($resultat!=0)
            {
                return true;
            }
            return false;
        }

        

        ?>




        <br><br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br>
        <br><br><br><br><br><br><br><br>

        <div id="footer">
            <?php
            include ("footer.php");
            ?>
        </div>
</body>
</html>