<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	
	<title>
		Dons de sang
	</title>
	<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Gem style -->
	<link rel="stylesheet" type="text/css" href="styles/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="styles/gabarit01.css" media="screen" />
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/font-awesome.css">
    <script src="js/modernizr.js"></script> <!-- Modernizr -->
  	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="js/jquery-3.1.0.min.js"></script>
	<script src="js/main.js"></script>
	
	
	<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">



</head>

<body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="bootstrap/dist/js/bootstrap.js"></script>
<div id="global">


	<div id="entete">
	<div>
			
				<div class="principal">
					<div class="principal_petit">
						<div class="principal_img">
							<a href="https://plus.google.com">
							<img src="google2.png" border="0" align="center" width="98%"/>
							</a>
						</div>
					</div>
				</div>

				
			
		</div>
		
		<div style="position: absolute; left: 90px; top: 15px">
			
				<div class="principal">
					<div class="principal_petit">
						<div class="principal_img">
							<a href="http://www.facebook.com">
							<img src="facebook2.png" border="0" align="center" width="98%"/>
							</a>
						</div>
					</div>
				</div>
			
		</div>
		
		<div style="position: absolute; left: 160px; top: 15px">
			
				<div class="principal">
					<div class="principal_petit">
						<div class="principal_img">
							<a href="http://www.instagram.com">
							<img src="insta.png" border="0" align="center" width="99%"/>
							</a>
						</div>
					</div>
				</div>
			
		</div>
    <!--
        <div style="position: absolute; left: 230px; top: 15px">
            <a href="admin.php">
                <img src="admin.png" width="15%">
            <input type="button" value="Espace Administrateur">
            </a>
        </div>
    -->
		<!-- place pour inscription et connexion -->
		
		
		
		<nav class="main-nav">
			<ul>
				<!-- inser more links here -->
				<li id="test"><a class="cd-signin" href="#0">Se Connecter</a></li>
				<li id="test"><a class="cd-signup" href="#0">S'inscrire</a></li>
				
				
<script>
			function YNconfirm() { 
     if (window.confirm('Accès interdit au public, continuer ?')){
         
         //REDIRECTION VERS LOGIN.PHP
         window.location.href = "administration/login.php";
     }
     else{
        //RESTE SUR LA MEME PAGE

        return false;
     }
}
</script>
				
                <li><a onclick="YNconfirm()" class="cd-signup1" href="javascript:void(0);" data-dialog="somedialog" class="trigger btn btn-lg btn-success" style="color: white !important;"><img src="admin.png" width="10%;"> Espace admin</a></li>
                
                	
                
			</ul>
		</nav>	
		

	<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			<ul class="cd-switcher">
				<li id="test"><a href="#0">Se Connecter</a></li>
				<li id="test" style="width: 250px"><a href="#0">Nouveau Compte</a></li>
			</ul>

			<div id="cd-login"> <!-- log in form -->
				<form class="cd-form" method="post" action="connex.php">
					<p class="fieldset">
						<label class="image-replace cd-email" for="signin-email">E-mail</label>
						<input class="full-width has-padding has-border" id="signin-email" type="email" name='mel' placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Mot de Passe</label>
						<input class="full-width has-padding has-border" id="signin-password" type="password" name="pwd" placeholder="Mot de Passe">
						<a href="#0" class="hide-password">Show</a>
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width" type="submit" value="Connexion" name="ok">
					</p>
				</form>
	
				
				
				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-login -->

			<div id="cd-signup"> <!-- sign up form -->
				<form class="cd-form" method="post" action="accueil.php">
					<p class="fieldset">
						<label class="image-replace cd-username" for="signup-nom">Nom</label>
						<input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Nom" name='nomdonneur' required >
					
					</p>
					
					<p class="fieldset">
						<label class="image-replace cd-username" for="signup-prenom">Prenom</label>
						<input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Prenom" name='prenomdonneur' required>
					
					</p>
                    
                    <p class="fieldset">
						<label class="image-replace cd-username" for="signup-age">Date de Naissance</label>
						<input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Date de Naissance (Format JJ/MM/AAAA)" name='ddn' required>
					</p>
					
					<p class="fieldset">
						<select  class="full-width has-padding has-border" id="signup-sexe" value="Sexe" placeholder="Sexe" name='sexe' >
                            <option value="Sexe">Sexe</option>
                            <option value="Masculin">Masculin</option>
                            <option value="F&eacute;minin">Féminin</option>
                        </select>
						
					</p>
					
					<p class="fieldset">
						<select  class="full-width has-padding has-border" id="signup-sexe" value="Groupe Sanguin" placeholder="Groupe Sanguin" name='groupe' >
                            <option value="Sexe">Groupe Sanguin</option>
                            <option value="Groupe A+">Groupe A+</option>
                            <option value="Groupe A-">Groupe A-</option>
							<option value="Groupe B+">Groupe B+</option>
							<option value="Groupe B-">Groupe B-</option>
							<option value="Groupe AB+">Groupe AB+</option>
							<option value="Groupe AB-">Groupe AB-</option>
							<option value="Groupe O+">Groupe O+</option>
							<option value="Groupe O-">Groupe O-</option>
                        </select>
						
					</p>
					
					

					<p class="fieldset">
						<label class="image-replace cd-email" for="signup-email">E-mail</label>
						<input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail" name='mel' required>
					
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signup-password">Mot de Passe</label>
						<input class="full-width has-padding has-border" id="signup-password" type="password" placeholder="Mot de Passe" name='pwd' required>
						<a href="#0" class="hide-password">Show</a>
						
					</p>
                    <p class="fieldset">
						<label class="image-replace cd-password" for="signup-password">Confirmer Mot de Passe</label>
						<input class="full-width has-padding has-border" id="signup-password" type="password" placeholder="Confirmer Mot de Passe" name='repeatpwd' required>
						<a href="#0" class="hide-password">Show</a>
					
					</p>
                    
                   
                   <p class="fieldset">
						<label class="image-replace cd-username" for="Telephone">Téléphone</label>
						<input class="full-width has-padding has-border" id="signup-telephone" type="number"  placeholder="Téléphone" name='tel' required>
					
					</p>
					 <p class="fieldset">
						<label class="image-replace cd-username" for="Adresse">Adresse</label>
						<input class="full-width has-padding has-border" id="signup-adresse" type="text"  placeholder="Adresse" name='adresse'>
					
					</p>
					

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" value="Créer nouveau compte" name='inscrire'>
					</p>
				</form>

			</div> <!-- cd-signup -->
	<!-- php de l'inscription -->
	
	
	
	<?php 
$BD= new PDO("mysql:host=localhost;dbname=dondesang",'root','Moimeme2018');
if(isset($_POST['inscrire']))
{
	if(validation($_POST['nomdonneur']) && validation($_POST['prenomdonneur']) && validation($_POST['ddn']) && validation($_POST['groupe']) && validation($_POST['tel']) && validation($_POST['mel']) && validation($_POST['pwd']) && validation($_POST['repeatpwd']) && validation($_POST['adresse']))
	{
		if($_POST['pwd']===$_POST['repeatpwd'])
		{		


			if(!is_inmy_db($BD,$_POST['mel']))
			{
						$req='INSERT INTO donneurs(nom_donneur,prenom_donneur,date_de_naissance,email,mot_de_passe,sexe,tel,adresse,groupe_sanguin) VALUES(?,?,?,?,sha1(md5(?)),?,?,?,?)';
						$requete=$BD->prepare($req);
						$requete->execute(array($_POST['nomdonneur'],$_POST['prenomdonneur'],$_POST['ddn'],$_POST['mel'],$_POST['pwd'],$_POST['sexe'],$_POST['tel'],$_POST['adresse'],$_POST['groupe']));
						if($requete)
						{

						?>
						<script>
							alert('Inscription réussie');
						</script>
						<?php
	   			 		echo '<meta http-equiv="refresh" content="0; url=accueil.php">';
						exit ();
        				}
        		
					
			}else
			?>
			<script>alert('Votre email est déjà existant dans la base données');</script>
			<?php
			echo '<meta http-equiv="refresh" content="0; url=accueil.php">';
			exit ();
		}else
		?>
		<script>alert('Les mots de passe entrés sont différents');</script>
		<?php
	echo '<meta http-equiv="refresh" content="0; url=accueil.php">';
	exit ();
	}
}

function is_inmy_db(PDO $base,$mail)
{
      
      $requet='SELECT id FROM donneurs where email=?';
      $r = $base->prepare($requet);
      $r->execute(array($mail));
      $resultat=$r-> rowCount();
   
      if($resultat!=0)
      {
            return true;
      }
      return false;

}
 
 function validation($val)
{
	if(isset($val)&& !empty($val))
	{
		return true;

	}
	return false;
 
}
?>
	<!-- fin php de l'inscription -->
		
			
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->
		
		
		
		<!-- fin inscription et connexion -->
		
		
	</div>
	
	
	<div id="navigation">
		<a href="accueil.php"><img src="sang.jpg" width="8%" style="position: absolute; left: 25px ;top : 100px"></a>
		
		<style>
.button {
	position: relative;
	float: left;
	left: 0;
    background-color: red;
    border: none;
    color: white;
    padding: 0.5em;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 10px 35px;
    -webkit-transition-duration: 0.4s;
    transition-duration: 0.4s;
    cursor: pointer;
    border-radius: 25px;
}


.button1 {
    background-color: white; 
    color: black; 
    border: 2px solid #D40015;
    border-radius: 25px;
}

.button1:hover {
    background-color: #D40015;
    color: white;
}


.buttonACC:enabled{
	  color: white;
	  background-color: #D40015;
}

</style>
		
	<div style="position: absolute; top: 4%; font-family: sans-serif">
		<a href="accueil.php"><button class="button button1 buttonACC">ACCUEIL</button></a>
		<a href="information.php"><button class="button button1">INFOS</button></a>
		<a href="event.php"><button class="button button1">EVENEMENTS</button></a>
		<a href="alertes.php"><button class="button button1">ALERTES</button></a>
		<a href="#footer"><button class="button button1">A PROPOS</button></a>
	</div>
	

	<?php
	
	include ("navigation.php");
	?>
	
	<div id="contenu">
		
		<div class="slideshow" style="position: absolute; right: 24px">
		<ul class="slider">
			<li>
				<img src="img/17.jpg">
				
			</li>
			<li>
				<img src="img/02.jpg">
				
			</li>
			
			<li>
				<img src="img/dondusang.jpg">
				
			</li>
			<li>
				<img src="img/test1.png">
				
			</li>
			<li>
				<img src="img/img.jpg">
				
			</li>
				<li>
				<img src="img/1.jpg">
				
			</li>
			<li>
				<img src="img/5.jpg">
				
			</li>
		</ul>

		<ol class="pagination">
			
		</ol>
	
		<div class="left">
			<span class="fa fa-chevron-left"></span>
		</div>

		<div class="right">
			<span class="fa fa-chevron-right"></span>
		</div>

	</div>
		
	 
		 
	</div><!-- #contenu -->
	
	<br><br><br><br><br>
		 <p align="center"><b>
			<span style="Font-Family: Times New Roman; font-size: 30px; color: grey" >P R E S E N T A T I O N</span>
		 </b>
		 </p>
		 <br>
		<p align="center" style="font-size : 25px; color: grey">La plateforme a pour objectif principal de promouvoir
		consid&eacute;rablement les <br> dons de sang. Elle se base sur des appels, des notifications et des <br> demandes pour les centres
		et hopitaux. L'application incite <br> aussi les donneurs avec des annonces et affiches.</p>
				<br><br><br>
	<table><img src="news.png" width="100%" align="center">

	
    </table><br><br><br><br><br><br><br><br><br>
    <h2 align="center">
		 Les centres de transfusions au Sénégal
		</h2>
    <br><br><br><br><br><br><br><br>
	 
	 <br>
	
	<img src="cnts.png" style="margin-left: 70px"><br><br><br>
	<div> 
	 <p style="font-size:20px; margin-left: 70px">Le Centre National de Transfusion Sanguine (CNTS) de Dakar est un<br> établissement public de santé doté d'une importante banque de sang. <br>Il organise fréquemment des journées de don de sang et dispose<br> d’un laboratoire d’analyses médicales ainsi que d’un service d’hématologie.<br>Le don de sang est un acte volontaire, bénévole et non rémunéré<br> destiné à sauver des vies humaines. </p>
	 </div>
	 <br><br><br><br><br><br><br><br>
	 
<div id="footer">
	<?php
		include ("footer.php");
	?>
</div>
</body>
</html>
