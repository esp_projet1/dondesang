<?php
session_start();
require_once 'administration/database.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	
	<title>
		Dons de sang
	</title>
	<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="css/style.css"> <!-- Gem style -->
	<link rel="stylesheet" type="text/css" href="styles/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="styles/gabarit01.css" media="screen" />
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/font-awesome.css">
      <script src="js/modernizr.js"></script> <!-- Modernizr -->
  	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="js/jquery-3.1.0.min.js"></script>
	<script src="js/main.js"></script>

</head>

<body>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<div id="global">


	<div id="entete">
		<div>
			
				<div class="principal">
					<div class="principal_petit">
						<div class="principal_img">
							<a href="https://plus.google.com">
							<img src="google2.png" border="0" align="center" width="98%"/>
							</a>
						</div>
					</div>
				</div>

				
			
		</div>
		
		<div style="position: absolute; left: 90px; top: 15px">
			
				<div class="principal">
					<div class="principal_petit">
						<div class="principal_img">
							<a href="http://www.facebook.com">
							<img src="facebook2.png" border="0" align="center" width="98%"/>
							</a>
						</div>
					</div>
				</div>
			
		</div>
		
		<div style="position: absolute; left: 160px; top: 15px">
			
				<div class="principal">
					<div class="principal_petit">
						<div class="principal_img">
							<a href="http://www.instagram.com">
							<img src="insta.png" border="0" align="center" width="99%"/>
							</a>
						</div>
					</div>
				</div>
			
		</div>
		<span style="position: absolute; right: 550px; top: 5px; font-family: arial">
			<h2 style="color: white">
				Planifiez vos dons
			</h2>
		</span>
		
		<nav class="main-nav">
			<ul>
				<!-- inser more links here -->
				<li id="test"><a class="cd-signup" href="#0">Gérer Compte</a></li>
				<li id="test"><a class="cd-signup" href="deconnexion.php">Déconnexion</a></li>
            
                	
                
			</ul>
		</nav>	
		
	</div> <!-- Fin entete -->
<div id="navigation">
	<a href="profil.php"><img src="sang.jpg" width="8%" style="position: absolute; left: 25px ;top : 100px"></a>

    <style>
        .button {
            position: relative;
            float: left;
            left: 0;
            background-color: red;
            border: none;
            color: white;
            padding: 0.5em;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 10px 35px;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            cursor: pointer;
            border-radius: 25px;
        }


        .button1 {
            background-color: white;
            color: black;
            border: 2px solid #D40015;
            border-radius: 25px;
        }

        .button1:hover {
            background-color: #D40015;
            color: white;
        }


        .buttonAc:enabled{
            color: white;
            background-color: #D40015;
        }

    </style>


    <?php

    $req=$bd->query('SELECT * FROM decision');
    $DECISION=$req->fetchAll();
    foreach ($DECISION as $decision): ?>


	<div style="position: absolute; top: 125px; font-family: sans-serif">
		<a href="profil.php"><button class="button button1 buttonAc">ACCUEIL COMPTE</button></a>
		<a href="obtenirRDV.php"><button class="button button1">OBTENIR RENDEZ-VOUS</button></a>
		<a href="notif_donneur.php?id=<?=$decision['id'] ?>"><button class="button button1">CONSULTER NOTIFICATIONS</button></a>

	</div>
	
	
	<?php endforeach;

	?>
	
	<div align="center">
  <div style="width:275px; position: absolute; right: 3px; top: 130px">
    <div>
      <input type="text" name="user_search" placeholder="RECHERCHER" id="area_search">
      <button type="button" id="btn_search"><img src="search.png" width="40%"></button>
      
    </div>
  </div>
</div>
	<script>
		$(document).ready(function () { 
    $('#btn_search').click(function()
    {
      $("#area_search").css({
      "height": "50px",
      "border": "0",
      "transition": "all 0.2s ease-in",
      "visibility": "visible",
      "background-color": "#DD4545",
      "width": "200px"
      });
      $("#btn_search").css({
      "background-color": "#999999"
      });
    });
    $("#area_search").keypress(function(){
      $("#sous_search").css({
        "transition": "all 0.1s ease-in",
        "visibility": "visible",
        "height": "25px"
      });
  });
});
	</script>
	
</div>

	
	<div id="contenu">
        
        <?php
if (isset($_SESSION['mel'])  && !empty($_SESSION['mel']) )
	{
	 $BD= new PDO("mysql:host=localhost; dbname=dondesang", "root", "Moimeme2018",array(PDO::ATTR_ERRMODE=> PDO::ERRMODE_EXCEPTION));
		  $requet='SELECT * FROM donneurs where email=?';
	      $req = $BD->prepare($requet);
	      $req->execute(array($_SESSION['mel']));
	      $resultat=$req-> rowCount();
	   
	      if($resultat!=0)
	      {
	      
	      while($donnes = $req->fetch()){
			
			echo '<h2 style="position: absolute; top: 300px; left: 75px; color: grey">Accueil Donneur</h2><br>';
			?> <img src="team.png" width="80px" style="position:absolute; top: 270px; left: 330px"> <?php
	      	echo '<b><h4 style="position: absolute; right: 50px; top: 61px; color: grey"> Bienvenue '.$donnes['prenom_donneur'].' '.$donnes['nom_donneur'].'</h4></b>';
	      	?>
			<div align="center" style="position: relative; top: -100px; border: solid #E6837C" >
				<br><br><br>
			<table>
				<tr>
					<th></th>
					<th>
						<h2 align="center" style="color: #D81B0D">Profil</h2>
					</th>
					<th>
						<h2 align="center" style="color: #D81B0D">Historique Des Dons</h2>
					</th>
				</tr>
				<tr>
					<td style="font-size:20px">Prénom(s) et Nom:</td>
					<td align="center">
						<?php echo $donnes['prenom_donneur'].' '.$donnes['nom_donneur']; ?>
					</td>
					<td align="center" rowspan="5">
						xxx
					</td>
				</tr>
				<tr>
					<td style="font-size:20px">Date de Naissance:</td>
					<td align="center">
						<?php echo $donnes['date_de_naissance']; ?>
					</td>
					
				</tr>
				<tr>
					<td style="font-size:20px">Groupe Sanguin:</td>
					<td align="center">
						<?php echo $donnes['groupe_sanguin']; ?>
					</td>
					
					
				</tr>
				
				
				<tr>
					<td style="font-size:20px">Adresse:</td>
					<td align="center">
						<?php echo $donnes['adresse']; ?>
					</td>
					
				</tr>
				<tr>
					<td style="font-size:20px">E-mail:</td>
					<td align="center">
						<?php echo $donnes['email']; ?>
					</td>
					
				</tr>
			</table>
			<br><br><br>
			</div><br><br><br>
			<?php
	      	
	      	
	      }
	     }
	     
	 }   
	  	     
	/*}else
	{
	//tu rediriges l'utilisateur vers la page index.php avec header("Location:la source de ton index.php")

	}*/
			
	?>
       
       
       
       
        <br><br><br><br><br><br><br><br><br>
        
        
     <br><br><br><br><br><br><br><br>
	 

</body>
</html>
