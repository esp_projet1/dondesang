<?php
session_start();
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/07/17
 * Time: 21:44
 */
require_once 'database.php';
require_once 'function.php';
$donneurs=getDonneur($bd,1,$_GET['id']);


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<div>


    <h1>
        <?= $donneurs -> prenom_donneur ?>
    </h1>
    <h1>
        <?= $donneurs -> nom_donneur ?>
    </h1>
    <h1>
        <?= $donneurs -> groupe_sanguin ?>
    </h1>

</div>


<?php
if (isset($_SESSION['admin']) and !empty($_SESSION['admin'])): ?>
<div>
    <a href="delete_donneur.php?id=<?= $donneurs-> id ?>"> Supprimer donneur</a>
    <a href="modify_donneur.php?id=<?= $donneurs-> id ?>">Modifier donneur</a>

    <a href="index.php">Espace admin</a>
</div>

<?php endif; ?>

</body>
</html>
