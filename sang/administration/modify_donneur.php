<?php
session_start();
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>

<?php

require_once 'database.php';
require_once 'function.php';
$donneurs=getDonneur($bd,1,$_GET['id']);
if (!isset($_GET['id'])) {
    header('Location: index.php');
}

if (!isset($_SESSION['admin']) || empty($_SESSION['admin'])) {
    header('Location: index.php');
}

if (isset($_POST) and !empty($_POST)) {
    if (!empty($_POST['prenom_donneur']) and !empty($_POST['nom_donneur'])  and !empty($_POST['email'])  and !empty($_POST['adresse'])) {
    $req=$bd->prepare('UPDATE donneurs SET prenom_donneur= :prenom_donneur, nom_donneur= :nom_donneur,  email= :email, adresse= :adresse  WHERE id= :id');
    $req->execute(array(
       'prenom_donneur' => $_POST['prenom_donneur'],
        'nom_donneur' => $_POST['nom_donneur'],
        'email' => $_POST['email'],
        'adresse' => $_POST['adresse'],
        'id' => $_GET['id']));

    if($req) {
        ?> <script>
            alert('Utilisateur modifié');
            </script>
        <?php
        echo '<meta http-equiv="refresh" content="0; url=index.php">';
        exit();

    }
    else {
        ?>
        <script>
            alert('Erreur !');
        </script>
        <?php
        echo '<meta http-equiv="refresh" content="0; url=index.php">';
        exit();
    }

    }
}

/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/07/17
 * Time: 22:57
 */


?>

<h2>Modification</h2>
<h3>Laissez vide si aucun changement</h3>
    <form method="post">
        <h3>
            Prénom:
        </h3>
        <input type="text" name="prenom_donneur" value="<?= $donneurs -> prenom_donneur ?>"/>
        <h3>
            Nom:
        </h3>
        <input type="text" name="nom_donneur" value="<?= $donneurs -> nom_donneur ?>"/>
        <h3>
            Email:
        </h3>
        <input type="text" name="email" value="<?= $donneurs -> email ?>"/>
        <h3>
            Adresse
        </h3>
        <input type="text" name="adresse" value="<?= $donneurs -> adresse ?>"/>

        <button>
            Modifier
        </button>


    </form>

</body>
</html>
