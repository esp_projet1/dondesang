<?php
session_start();
require_once 'database.php';

/**
 * Created by PhpStorm.
 * User: root
 * Date: 18/07/17
 * Time: 20:32
 */

    if (isset($_POST) and !empty($_POST)) {
        if(!empty(htmlspecialchars($_POST['pseudo'])) and !empty(htmlspecialchars($_POST['mdp']))) {
            $req1=$bd->prepare('SELECT * from admin WHERE pseudo=:pseudo and mdp=:mdp');
            $req1->execute([
               'pseudo' => $_POST['pseudo'],
                'mdp' => $_POST['mdp'],
            ]);
            $user=$req1->fetchObject();
                if ($user) {
                    $_SESSION['admin']=$_POST['pseudo'];
                    header('Location: index.php');
                }
                else {
                    ?>
                    <script>
                        alert('L\'identifiant est incorrect');
                    </script>
                    <?php
                    echo '<meta http-equiv="refresh" content="0; url=login.php">';

                }
        }
        else {
            $error='Veuillez remplir tous les champs';
        }
    }
    if (isset($error)) {
        echo '<div>'.$error.'</div>';
    }

    ?>

