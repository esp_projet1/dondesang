
<?php
session_start();
require_once 'database.php';
if(!$_SESSION['admin']) {
    header('Location: login.php');
}



echo '<a href="deconnexion.php">Deconnexion</a>';
?>

<html xmlns:top="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <link  rel="stylesheet" href="app.css">

</head>
<body>

<div class="sidebar">
    <div class="title">
        <img src="perso_red.png" width="100px" height="80px">
        <p>
          ADMIN
        </p>
    </div>
    <ul class="nav">
        <a href="index.php">
            <li class="nav-item">
                <img src="dashboard.png" width="30px">
                <p style="position:absolute; left: 60px; top: 95px">Dashboard</p>
            </li>
        </a>
        <a href="add_event.php">
            <li class="nav-item">
                <img src="event.png" width="30px">
                <p style="position:absolute; left: 60px; top: 170px">Événements</p>
            </li>
        </a>
        <a href="add_alert.php">
            <li class="nav-item">
                <img src="alert.png" width="30px">
                <p style="position:absolute; left: 60px; top: 245px">Lancer Alertes</p>
            </li>
        </a>
        <a href="add_donneur.php">
            <li class="nav-item">
                <img src="add.png" width="30px">
                <p  style="position:absolute; left: 60px; top: 320px">Ajouter users</p>
            </li>
        </a>
        <a href="notif_admin.php">
            <li class="nav-item">
                <img src="msg.png" width="30px">
                <p  style="position:absolute; left: 60px; top: 395px">Messagerie</p>
            </li>
        </a>
        <a href="add_admin.php">
            <li class="nav-item">
                <img src="add.png" width="30px">
                <p  style="position:absolute; left: 60px; top: 470px">Ajouter Admin</p>
            </li>
        </a>


    </ul>





</div>


<?php
include('entete.php');

?>

<div class="centre" style="position:absolute; left: 15%; top: 90px; right: 0">


    <ul><a href="deconnexion.php">
            <li style="float: right; height: 180px; margin-top: 15px; margin-left: 30px; margin-right: 40px; position: relative; cursor:pointer"><img src="logout.jpg" width="30px" title="Déconnexion"></li></a>
        <li style="float: right; height: 180px; margin-top: 15px; margin-left: 30px; margin-right: 5px; position: relative; cursor:pointer"><img src="edit.png" width="34px" title="Éditer profil Administrateur"></li>
    </ul>


</div>

<a href="add_event.php">
    <img src="time1.png" width="115px" style="position:absolute; left: 515px; top: 95px">
</a>
<a href="add_alert.php">
    <img src="megaphone.png" width="115px" style="position:absolute; left: 695px; top: 95px">
</a>
<a href="add_donneur.php">
    <img src="profiles.png" width="115px" style="position:absolute; left: 875px; top: 95px" title="Ajouter un Utilisateur">
</a>
<br>

<a href="index.php">
    <div style="position: absolute; top: 100px; margin-left: 250px ">
        <img src="home.jpg" width="45px">
        <span style="position: absolute; top: 15px; color: #626365; font-family:'Montserrat'; cursor:pointer;">Accueil</span>
    </div>
</a>



<?php
require_once 'database.php';
require_once 'function.php';

if(!isset($_SESSION['admin']) || empty($_SESSION['admin'])) {
    header('Location: index.php');
}

if(isset($_POST) and !empty($_POST)) {
    if (!empty($_POST['libelle_event']) and !empty($_POST['date_event']) and !empty($_POST['lieu_event']) and !empty($_POST['commentaire']) and !empty($_POST['type_event'])) {
        $req =$bd -> prepare('INSERT INTO evenements (libelle_event, date_event ,lieu_event ,commentaire ,type_event) VALUES (:libelle_event, :date_event,:lieu_event, :commentaire, :type_event)');
        $req->execute(array(
            'libelle_event' => $_POST['libelle_event'],
            'date_event' => $_POST['date_event'],
            'lieu_event' => $_POST['lieu_event'],
            'commentaire' => $_POST['commentaire'],
             'type_event' => $_POST['type_event']));

        if ($req) {
            ?>
            <script>
                alert('Evenement Ajouté dans la plateforme!');
            </script>
        <?php
        echo '<meta http-equiv="refresh" content="0; url=index.php">';
        exit();
        }
        else {
        ?>
            <script>
                alert('Erreur evenement non ajoutée! ');
            </script>
            <?php
            echo '<meta http-equiv="refresh" content="0; url=index.php">';
            exit();
        }


    }
}

?>
<p style="position: absolute; color: #626365; left: 650px; top: 275px; font-size:15px;">Organiser un evenement </p>
<!--  FORMULAIRE AJOUTER EVENT  -->
<form method="post" style="position:absolute; left: 500px; margin-top:350px;"  class="form-style-9">
    <ul>

        <li>
            <select value="type_event" class="field-style field-full align-none" placeholder="type d'evenement" name='type_event' style="width: 400px; margin-left: 50px; margin-top:30px;" >
                <option value="type d'evenement"> Type d'Evénement</option>
                <option value="Sensibilisation">Journée de Sensibilisation</option>
                <option value="Collecte ">Journée de Collecte </option>
                <option value="Rencontre"> Journée de Rencontre </option>
            </select>
        </li>



        <li>
            <input type="text" name="libelle_event" class="field-style field-split align-left" placeholder="Libelle Evénement" style="width: 400px; margin-left: 50px; margin-top:10px;">

        </li>

        <li>
            <input type="text" name="date_event" class="field-style field-split align-left" placeholder="Date Evénement Format JJ/MM/AAAA" style="width: 400px; margin-left:50px; margin-top:10px;">

        </li>
        <li>
            <input type="text" name="lieu_event" class="field-style field-split align-left" placeholder="Lieu Evénement" style="width: 400px; margin-left:50px; margin-top:10px;">

        </li><br><br>
        <span style="font-family:Montserrat; color: #626365; margin-left: 50px">
            Objectif de l'Evénement:
             <br>
                </span>

        <li>

            <textarea name="commentaire" class="field-style field-split align-left" cols="190" style="width: 400px ; margin-left: 50px; margin-top: 20px;">
            </textarea>
        </li>








        <br>

        <br>
        <li>
            <button style="margin-left: 180px; width: 150px; height: 40px; background-color: #3498db; color: #fff; border-radius: 3px; border: 0; font-size: 18px; cursor: pointer;">Valider</button>
        </li>
    </ul>
</form>


<style type="text/css">
    .form-style-9{
        width: 510px;
        background: #FAFAFA;1
        padding: 30px;
        margin: 50px auto;
        box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
        border-radius: 10px;
        border: 6px solid #305A72;
        height: 510px;
    }
    .form-style-9 ul{
        padding:0;
        margin:0;
        list-style:none;
    }
    .form-style-9 ul li{
        display: block;
        margin-bottom: 10px;
        min-height: 35px;
    }
    .form-style-9 ul li  .field-style{
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        padding: 8px;
        outline: none;
        border: 1px solid #B0CFE0;
        -webkit-transition: all 0.30s ease-in-out;
        -moz-transition: all 0.30s ease-in-out;
        -ms-transition: all 0.30s ease-in-out;
        -o-transition: all 0.30s ease-in-out;

    }.form-style-9 ul li  .field-style:focus{
         box-shadow: 0 0 5px #B0CFE0;
         border:1px solid #B0CFE0;
     }
    .form-style-9 ul li .field-split{
        width: 49%;
    }
    .form-style-9 ul li .field-full{
        width: 100%;
    }
    .form-style-9 ul li input.align-left{
        float:left;
    }
    .form-style-9 ul li input.align-right{
        float:right;
    }
    .form-style-9 ul li textarea{
        width: 100%;
        height: 100px;
    }
    .form-style-9 ul li input[type="button"],
    .form-style-9 ul li input[type="submit"] {
        -moz-box-shadow: inset 0px 1px 0px 0px #3985B1;
        -webkit-box-shadow: inset 0px 1px 0px 0px #3985B1;
        box-shadow: inset 0px 1px 0px 0px #3985B1;
        background-color: #216288;
        border: 1px solid #17445E;
        display: inline-block;
        cursor: pointer;
        color: #FFFFFF;
        padding: 8px 18px;
        text-decoration: none;
        font: 12px Arial, Helvetica, sans-serif;
    }
    .form-style-9 ul li input[type="button"]:hover,
    .form-style-9 ul li input[type="submit"]:hover {
        background: linear-gradient(to bottom, #2D77A2 5%, #337DA8 100%);
        background-color: #28739E;
    }
</style>


<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
</script>
<script src="app.js"></script>
</body>
</html>
