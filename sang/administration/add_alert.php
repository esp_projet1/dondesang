
<?php
session_start();
require_once 'database.php';
if(!$_SESSION['admin']) {
    header('Location: login.php');
}



echo '<a href="deconnexion.php">Deconnexion</a>';
?>

<html xmlns:top="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <link  rel="stylesheet" href="app.css">

</head>
<body>

<div class="sidebar">
    <div class="title">
        <img src="perso_red.png" width="100px" height="80px">
        <p>
            ADMIN
        </p>
    </div>
    <ul class="nav">
        <a href="index.php">
            <li class="nav-item">
                <img src="dashboard.png" width="30px">
                <p style="position:absolute; left: 60px; top: 95px">Dashboard</p>
            </li>
        </a>
        <a href="add_event.php">
            <li class="nav-item">
                <img src="event.png" width="30px">
                <p style="position:absolute; left: 60px; top: 170px">Événements</p>
            </li>
        </a>
        <a href="add_alert.php">
            <li class="nav-item">
                <img src="alert.png" width="30px">
                <p style="position:absolute; left: 60px; top: 245px">Lancer Alertes</p>
            </li>
        </a>
        <a href="add_donneur.php">
            <li class="nav-item">
                <img src="add.png" width="30px">
                <p  style="position:absolute; left: 60px; top: 320px">Ajouter users</p>
            </li>
        </a>
        <a href="notif_admin.php">
            <li class="nav-item">
                <img src="msg.png" width="30px">
                <p  style="position:absolute; left: 60px; top: 395px">Messagerie</p>
            </li>
        </a>
        <a href="add_admin.php">
            <li class="nav-item">
                <img src="add.png" width="30px">
                <p  style="position:absolute; left: 60px; top: 470px">Ajouter Admin</p>
            </li>
        </a>


    </ul>





</div>


<?php
include('entete.php');

?>

<div class="centre" style="position:absolute; left: 15%; top: 90px; right: 0">


    <ul><a href="deconnexion.php">
            <li style="float: right; height: 180px; margin-top: 15px; margin-left: 30px; margin-right: 40px; position: relative; cursor:pointer"><img src="logout.jpg" width="30px" title="Déconnexion"></li></a>
        <li style="float: right; height: 180px; margin-top: 15px; margin-left: 30px; margin-right: 5px; position: relative; cursor:pointer"><img src="edit.png" width="34px" title="Éditer profil Administrateur"></li>
    </ul>


</div>

<a href="add_event.php">
    <img src="time1.png" width="115px" style="position:absolute; left: 515px; top: 95px">
</a>
<a href="add_alert.php">
    <img src="megaphone.png" width="115px" style="position:absolute; left: 695px; top: 95px">
</a>
<a href="add_donneur.php">
    <img src="profiles.png" width="115px" style="position:absolute; left: 875px; top: 95px" title="Ajouter un Utilisateur">
</a>
<br>

<a href="index.php">
    <div style="position: absolute; top: 100px; margin-left: 250px ">
        <img src="home.jpg" width="45px">
        <span style="position: absolute; top: 15px; color: #626365; font-family:'Montserrat'; cursor:pointer;">Accueil</span>
    </div>
</a>



<?php
require_once 'database.php';
require_once 'function.php';

if(!isset($_SESSION['admin']) || empty($_SESSION['admin'])) {
    header('Location: index.php');
}

if(isset($_POST) and !empty($_POST)) {
    if (!empty($_POST['libelle']) and !empty($_POST['contenu']) and !empty($_POST['date_alerte']) and !empty($_POST['groupe'])) {
        $req =$bd -> prepare('INSERT INTO alerte (libelle, contenu,date_alerte, groupe) VALUES (:libelle, :contenu,:date_alerte, :groupe)');
        $req->execute(array(
            'libelle' => $_POST['libelle'],
            'contenu' => $_POST['contenu'],
            'date_alerte' => $_POST['date_alerte'],
            'groupe' => $_POST['groupe']));
        if ($req) {
            ?>
            <script>
                alert('Alerte Ajoutée dans la plateforme!');
            </script>
        <?php
        echo '<meta http-equiv="refresh" content="0; url=index.php">';
        exit();
        }
        else {
        ?>
            <script>
                alert('Erreur alerte non ajoutée! ');
            </script>
            <?php
            echo '<meta http-equiv="refresh" content="0; url=index.php">';
            exit();
        }


    }
}

?>
<p style="position: absolute; color: #626365; left: 650px; top: 275px; font-size:15px;">Lancer une Alerte</p>
<!--  FORMULAIRE AJOUTER ALERTES   -->
<form method="post" style="position:absolute; left: 490px; margin-top:350px;"  class="form-style-9">
    <ul>
        <li>
            <input type="text" name="libelle" class="field-style field-split align-left" placeholder="Libelle Alerte" style="width: 450px">

        </li>
        <br>
        <span style="font-family:Montserrat; color: #626365;">
            Descriptif de l'alerte:
             <br>
                </span><br>

        <li>

            <textarea name="contenu" class="field-style field-split align-left" cols="190" style="width: 450px">
            </textarea>
        </li>
        <li>
            <input type="text" name="date_alerte" class="field-style field-split align-left" placeholder="Date Alerte" style="width: 450px">
        </li>


        <li>
            <select value="Groupe Sanguin" class="field-style field-full align-none" placeholder="Groupe Sanguin" name='groupe' >
                <option value="Sexe">Groupe Sanguin</option>
                <option value="Groupe A+">Groupe A+</option>
                <option value="Groupe A-">Groupe A-</option>
                <option value="Groupe B+">Groupe B+</option>
                <option value="Groupe B-">Groupe B-</option>
                <option value="Groupe AB+">Groupe AB+</option>
                <option value="Groupe AB-">Groupe AB-</option>
                <option value="Groupe O+">Groupe O+</option>
                <option value="Groupe O-">Groupe O-</option>
            </select>
        </li>
        <br>

        <br>
        <li>
            <button style="margin-left: 150px; width: 150px; height: 40px; background-color: #3498db; color: #fff; border-radius: 3px; border: 0; font-size: 18px; cursor: pointer;">Valider</button>
        </li>
    </ul>
</form>


<style type="text/css">
    .form-style-9{
        max-width: 450px;
        background: #FAFAFA;
        padding: 30px;
        margin: 50px auto;
        box-shadow: 1px 1px 25px rgba(0, 0, 0, 0.35);
        border-radius: 10px;
        border: 6px solid #305A72;
        height: 380px;
    }
    .form-style-9 ul{
        padding:0;
        margin:0;
        list-style:none;
    }
    .form-style-9 ul li{
        display: block;
        margin-bottom: 10px;
        min-height: 35px;
    }
    .form-style-9 ul li  .field-style{
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        padding: 8px;
        outline: none;
        border: 1px solid #B0CFE0;
        -webkit-transition: all 0.30s ease-in-out;
        -moz-transition: all 0.30s ease-in-out;
        -ms-transition: all 0.30s ease-in-out;
        -o-transition: all 0.30s ease-in-out;

    }.form-style-9 ul li  .field-style:focus{
         box-shadow: 0 0 5px #B0CFE0;
         border:1px solid #B0CFE0;
     }
    .form-style-9 ul li .field-split{
        width: 49%;
    }
    .form-style-9 ul li .field-full{
        width: 100%;
    }
    .form-style-9 ul li input.align-left{
        float:left;
    }
    .form-style-9 ul li input.align-right{
        float:right;
    }
    .form-style-9 ul li textarea{
        width: 100%;
        height: 100px;
    }
    .form-style-9 ul li input[type="button"],
    .form-style-9 ul li input[type="submit"] {
        -moz-box-shadow: inset 0px 1px 0px 0px #3985B1;
        -webkit-box-shadow: inset 0px 1px 0px 0px #3985B1;
        box-shadow: inset 0px 1px 0px 0px #3985B1;
        background-color: #216288;
        border: 1px solid #17445E;
        display: inline-block;
        cursor: pointer;
        color: #FFFFFF;
        padding: 8px 18px;
        text-decoration: none;
        font: 12px Arial, Helvetica, sans-serif;
    }
    .form-style-9 ul li input[type="button"]:hover,
    .form-style-9 ul li input[type="submit"]:hover {
        background: linear-gradient(to bottom, #2D77A2 5%, #337DA8 100%);
        background-color: #28739E;
    }
</style>


<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
</script>
<script src="app.js"></script>
</body>
</html>
